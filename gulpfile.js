var gulp = require('gulp');
var concatCss = require('gulp-concat-css');
var concat = require('gulp-concat');
var htmlmin = require('gulp-htmlmin');
var uglify = require('gulp-uglify');
var runSequence = require('run-sequence');
var rimraf = require('rimraf');
var minifyCss = require('gulp-minify-css');
var debug = require('gulp-debug');
var replace = require('gulp-replace');
var inject = require('gulp-inject');
var awspublish = require('gulp-awspublish');
var copy = require('gulp-copy');



gulp.task('concat-css', function() {
  return gulp.src('./app-src/css/*.css')
    .pipe(concatCss("./css/bundle.css"))
    .pipe(gulp.dest('./dist'));
});


gulp.task('minify-html', function() {
  return gulp.src('./dist/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./dist'))
});


gulp.task('concat-scripts', function() {
  return gulp.src('./app-src/js/*.js')
    .pipe(concat('app.js'))
    .pipe(gulp.dest('./dist/js'));
});


gulp.task('minify-scripts', function() {
  return gulp.src('./dist/js/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('./dist/js'));
});

 
gulp.task('minify-css', function() {
  return gulp.src('./dist/css/*.css')
    .pipe(minifyCss({compatibility: 'ie8'}))
    .pipe(gulp.dest('./dist/css'));
});

 
gulp.task('inject', function () {
  var target = gulp.src('./app-src/index.html');
 
  return target.pipe(inject(gulp.src(['./dist/**/*.js', './dist/**/*.css'], 
  		{read: false}), 
  		{ignorePath: 'dist', addRootSlash: false}))
    .pipe(gulp.dest('./dist'));
});


gulp.task('copy-fonts', function () {
	return gulp.src('./app-src/font/**/*')
		.pipe(copy('dist', { prefix: 1 }));
});


gulp.task('copy-images', function () {
	return gulp.src('./app-src/*.png')
		.pipe(copy('dist', { prefix: 1 }));
});


gulp.task('copy-example-data', function () {
	return gulp.src('./app-src/example-data/*.json')
		.pipe(copy('dist', { prefix: 1 }));
});


gulp.task('publish', function() {
  var publisher = awspublish.create({
    params: {
      Bucket: 'tfl-tube-status-app'
    }
  });

  var headers = {
    'Cache-Control': 'max-age=315360000, no-transform, public'
  };
 
  return gulp.src([
  	'./dist/*',
  	'./dist/**/*'
  	])
    .pipe(publisher.publish(headers))
    .pipe(publisher.cache())
    .pipe(awspublish.reporter());
});


gulp.task('clean-dist', function(cb) {
    return rimraf('./dist', cb);
});


gulp.task('default', function() {
	runSequence('clean-dist', 'copy-fonts', 'copy-images', 'copy-example-data', 'concat-css', 'concat-scripts', 'minify-scripts', 'minify-css', 'inject', 'minify-html', 'publish');
});