//(function($){
//  $(function(){
      
    function loadStatus() {
        
        $('.icon-container').fadeTo('fast', 0.75);
        
        var dataUrl = "http://tfl-tube-status-api.apps.digitalindustria.com/api/status/all";
        
        if (location.hash !== "") {
            dataUrl = "http://tfl-tube-status-app.s3-website-eu-west-1.amazonaws.com/example-data/tfl-data-delay-example-" + location.hash.replace('#', '') + ".json";
        }        
        
        $.ajax({
            method: "GET",
            url: dataUrl
        })
        .done(function(statusData) {
            
            $('.icon-container').fadeTo('fast', 1);
            
            $.each(statusData, function(k, line) {
               
               if (line.lineStatuses[0].statusSeverityDescription !== 'Good Service') {
                   
                   // Something has happened on this line
                   $('#' + line.id + '-icon').text('announcement');
                   $('#' + line.id + '-status').text(line.lineStatuses[0].statusSeverityDescription).addClass('alert');
                   $('#' + line.id + '-reason').show().text(line.lineStatuses[0].reason);
                   
                   var wordCount = line.lineStatuses[0].reason.split(' ').length;
                   var quantifier = 2.6;
                   console.log(wordCount);
                   
                   if (wordCount >= 35) {
                       quantifier = 3;
                   }
                   
                   $('#' + line.id + '-reason').closest('.parallax-container').css('min-height', ((wordCount * quantifier) + 215) + 'px');
                   $('#' + line.id + '-reason').closest('.parallax-container').attr('data-index', 1);
                   
               } else {
                   
                   // Nothing to see here, reset if needed.
                   $('#' + line.id + '-icon').text('done');
                   $('#' + line.id + '-status').text(line.lineStatuses[0].statusSeverityDescription).removeClass('alert');
                   $('#' + line.id + '-reason').hide().text('');
                   
                   $('#' + line.id + '-reason').closest('.parallax-container').removeAttr('style');
                   $('#' + line.id + '-reason').closest('.parallax-container').attr('data-index', 0);
                   
               }
                
            });    
            
        });            
        
    }
      
    $(function() {
      
        $('.parallax').parallax();
        
        setInterval(function() {        
            loadStatus();
        }, 120000); // 2 minutes
        
        
        loadStatus();
    
    });

//  }); // end of document ready
// })(jQuery); // end of jQuery name space